function tf = nearZeroVar(x, freqCut, uniqueCut)
% Return true if column-vector x has near zero variance according to the
% definition in nearZeroVar function in R found here:
% https://www.rdocumentation.org/packages/caret/versions/6.0-77/topics/nearZeroVar

tf = false;

% Get unique values and indices mapping unique values onto the original
% vector
[ux,~,ix] = unique(x);

% If only one unique value, stop
if numel(ux)==1
    tf = true;
    return
end

% Get frequencies of unique values
freq = accumarray(ix,1);

% Sort
freq = sort(freq,'descend');

% If the ratio of frequencies of most to 2nd most frequent element is too
% high, stop
if freq(1)/freq(2) > freqCut
    tf = true;
    return
end

% % If the number of unique elements is too small compared to the total
% % number of elements, stop
% if numel(ux)/numel(x)*100 < uniqueCut
%     tf = true;
%     return
% end

end
