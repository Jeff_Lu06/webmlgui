function ScatterPlotPortion(app, feature,label, isTrain, CorrectLabel, valFeature, varName)
    % feature: Seleceted feature
    % label: label in Dataset
    % isTrain: examine whether it's used for training step
    % CorrectLabel: prediced label after validation
    % valFeature: validation feature, length must be same as CorrectLabel
    
    % Create folder and set up parameters
    cla(app.UIAxes, 'reset')
    app.UIAxes.YLimMode = 'auto';
    app.UIAxes.XLimMode = 'auto';
    app.UIAxes.Visible = 'off';
    app.UIAxes.Visible = 'on';
    ulabel = unique(label);
    % Plot
    if isTrain == false
        % Before training
        gscatter(app.UIAxes, feature(:,1), feature(:,2), label, 'rgbcmykw');
    else
        % After training
        f1 = valFeature(:,1);
        f2 = valFeature(:,2);
        % Get mispredicted data
        WFeature = [f1(~CorrectLabel) f2(~CorrectLabel)];
        gscatter(app.UIAxes, feature(:,1), feature(:,2), label, 'rgbcmykw');
        hold(app.UIAxes, 'on');
        plot(app.UIAxes, WFeature(:,1), WFeature(:,2),'x', 'MarkerSize', 10, 'MarkerEdgeColor', 'k');
        ulabel = [ulabel; 'Wrong'];
        hold(app.UIAxes, 'off');
    end
    xlabel(app.UIAxes, varName{1}, 'FontSize',12, 'Interpreter', 'none');
    ylabel(app.UIAxes, varName{2}, 'FontSize',12, 'Interpreter', 'none');
    lgd = legend(app.UIAxes, ulabel);
    lgd.FontSize = 12;
    grid(app.UIAxes, 'on')
    disableDefaultInteractivity(app.UIAxes)
end