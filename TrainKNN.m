function [Model, valAccuracy, CorrectLabel, valData, est_label, valLabel, dataTotal,...
    ROCbox, AUCs] = TrainKNN(app, data, rawData, valSwitch, selectedFeature, portion, ...
    k, valData, template, replacenan, normtype, needPCA, plotTest, needFillOut, stridx, catIdx)
    % data : Whole dataset
    % valSwitch : Use different method to get validtion
    % feature: Selected two features in GUI
    % TrainPlotName: filename for saved figure
    % portion: number of validation data/ whole data
    % k: k-Fold
    % valData: loaded validation data 

    fixNum = [];
    if ~isempty(stridx) && ~isequal(1:size(data, 2)-1, stridx)
        figure(app.InfinityAI2ML)
        msg = msgbox(['Training terminated. When using KNN, please make sure features are ' ...
            'all continuous or all categorical.']);
        waitfor(msg)
        return
    end
    try
        if valSwitch == 1
            cv = cvpartition(size(rawData,1), 'HoldOut', portion);
            idx = cv.test;
            trainData = rawData(~idx, :);
            valData = rawData(idx, :);
            valLabel = valData{:, end};
            [doneTrain, doneVal] = ValPreprocess(trainData, valData, replacenan, normtype, needPCA, ...
                stridx, fixNum, needFillOut);
            Model = fitcensemble(doneTrain, doneTrain.Properties.VariableNames{end}, 'Learners', template, ...
                'CategoricalPredictors', catIdx);
            [valPredict, posterior] = predict(Model, doneVal);
            CorrectLabel = strcmp(valPredict, valLabel);
            est_label = valPredict;
            valAccuracy = nnz(CorrectLabel)/size(valPredict,1);
            valPlot = doneVal(:, selectedFeature);
            dataTotal = [doneTrain; doneVal];
            valData = doneVal;
            if ~plotTest
                confusionchart(app.Panel_3, valLabel, est_label);
            end

            % ROC curve
            numClasses = length(Model.ClassNames);
            ROCbox = cell(1, numClasses);
            AUCs = zeros(1, numClasses);
            uValLabel = unique(doneVal{:, end});
            diffClass = setdiff(Model.ClassNames, uValLabel);
            for i = 1:numClasses
                if isempty(setdiff(Model.ClassNames{i}, diffClass))
                    ROCbox{i} = 'No data found';
                    AUCs(i) = nan;
                    continue
                end
                [xROC, yROC, ~, AUC] = perfcurve(valLabel, posterior(:, i), Model.ClassNames{i});
                ROCbox{i} = [xROC yROC];
                AUCs(i) = AUC;
            end

            % Actual model for testing (train with all data)
            Model = fitcensemble(data, data.Properties.VariableNames{end}, 'Learners', template, ...
                'CategoricalPredictors', catIdx);
        elseif valSwitch == 2
            % k-fold 
            cv = cvpartition(size(rawData, 1), 'KFold', k);
            ac = zeros(1, k);
            ModelTotal = cell(k, 1);
            valTotal = [];
            valPredictTotal = [];
            CorrectLabelTotal = [];
            fixNum = [];
            for i = 1:k
                train = rawData(cv.training(i),:);
                val = rawData(cv.test(i),:);
                if needPCA && i == 1
                    [doneTrain, doneVal, fixNumNew] = ValPreprocess(train, val, replacenan, normtype, needPCA, ...
                        stridx, fixNum, needFillOut);
                    fixNum = fixNumNew;
                elseif needPCA && i ~= 1
                    [doneTrain, doneVal, ~] = ValPreprocess(train, val, replacenan, normtype, needPCA, ...
                        stridx, fixNum, needFillOut);
                elseif ~needPCA 
                    [doneTrain, doneVal] = ValPreprocess(train, val, replacenan, normtype, needPCA, ...
                        stridx, fixNum, needFillOut); 
                end
                Model = fitcensemble(doneTrain, doneTrain.Properties.VariableNames{end}, 'Learners', template, ...
                    'CategoricalPredictors', catIdx);
                valPredict = predict(Model, doneVal);
                CorrectLabel = strcmp(valPredict, doneVal{:,end});
                ModelTotal{i} = Model;
                ac(i) =nnz(CorrectLabel)/size(doneVal, 1);
                valPredictTotal = [valPredictTotal; valPredict];
                valTotal = [valTotal; doneVal];
                CorrectLabelTotal = [CorrectLabelTotal; CorrectLabel];

            end
            % Get validation ROC parameters
            [~, bestidx] = min(ac);
            m = ModelTotal{bestidx};
            [~, posterior] = predict(m, doneVal);
            numClasses = length(m.ClassNames);
            ROCbox = cell(1, numClasses);
            AUCs = zeros(1, numClasses);
            uValLabel = unique(doneVal{:, end});
            diffClass = setdiff(m.ClassNames, uValLabel);
            for i = 1:numClasses
                if isempty(setdiff(m.ClassNames{i}, diffClass))
                    ROCbox{i} = 'No data found';
                    AUCs(i) = nan;
                    continue
                end
                [xROC, yROC, ~, AUC] = perfcurve(doneVal{:,end}, posterior(:, i), m.ClassNames{i});
                ROCbox{i} = [xROC yROC];
                AUCs(i) = AUC;
            end

            % Get CorrectLabel for the median accuracy index
            CorrectLabel = CorrectLabelTotal;
            valData = valTotal;
            dataTotal = valTotal;
            valTotal = valTotal(:,selectedFeature);
            est_label = valPredictTotal;
            valLabel = valData{:,end};
            valAccuracy = mean(ac);
            if ~plotTest
                confusionchart(app.Panel_3, valLabel, est_label);
            end
            % Actual model for testing (train with all data)
            Model = fitcensemble(data, data.Properties.VariableNames{end}, 'Learners', template, ...
                'CategoricalPredictors', catIdx);
        elseif valSwitch == 3
            valLabel = valData{:,end};
            valPlot = valData(:, selectedFeature);
            Model = fitcensemble(data, data.Properties.VariableNames{end}, 'Learners', template, ...
                'CategoricalPredictors', catIdx);
            [est_label, posterior] = predict(Model, valData);
            CorrectLabel = zeros(size(est_label));
            for i = 1: numel(est_label)
                CorrectLabel(i) = strcmp(est_label{i}, valLabel{i});
            end
            valAccuracy = nnz(double(CorrectLabel))/numel(est_label);
            dataTotal = [data; valData];
            if ~plotTest
                confusionchart(app.Panel_3, valLabel, est_label);
            end
            
            % ROC
            numClasses = length(Model.ClassNames);
            ROCbox = cell(1, numClasses);
            AUCs = zeros(1, numClasses);
            uValLabel = unique(valLabel);
            diffClass = setdiff(Model.ClassNames, uValLabel);
            for i = 1:numClasses
                if isempty(setdiff(Model.ClassNames{i}, diffClass))
                    ROCbox{i} = 'No data found';
                    AUCs(i) = nan;
                    continue
                end
                [xROC, yROC, ~, AUC] = perfcurve(valLabel, posterior(:, i), Model.ClassNames{i});
                ROCbox{i} = [xROC yROC];
                AUCs(i) = AUC;
            end
        end
        figure(app.InfinityAI2ML)
    catch
    end
end