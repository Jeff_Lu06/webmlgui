function [Model, Loss, rSquare, yPredict, yActual, yPredict3, yActual3, warn] = TrainLinReg(app, data, rawData, ...
    selectedNodes, valSwitch, portion, k, valData, robustCheck, stepCheck, modelSpec, stridx, replacenan, ...
    normtype, needPCA, needFillOut, catIdx)
    % data: training data
    % selectedNodes: selected predictors for training
    % valSwitch: use different methods to get validtion
        % valSwitch = 1, use partial training data as validation data
        % valSwitch = 2, use k-fold
        % valSwitch = 3, use uploaded validation data
    % portion: number of validation data/ whole data
    % k: k-fold
    % valData: loaded validation data 
% Model parameters:
    % robustCheck: check whether use robust 
    % stepCheck: check whether use step improvement 
    % modelSpec: terms, like linear, interactions, quadratic
    % stridx: index of string predictors
    % Create wait bar and keep it running
    lastwarn('');
    fixNum = [];
    try
        % Set up parameters for each validation method
        if valSwitch == 1
            cv = cvpartition(size(rawData, 1),'HoldOut', portion);
            idx = cv.test;
            trainData = rawData(~idx, :);
            valData = rawData(idx, :);
            [doneTrain, doneVal] = ValPreprocess(trainData, valData, replacenan, normtype, needPCA, stridx, fixNum, ...
                needFillOut);
        elseif valSwitch == 2
            rmse = [];
            rsquare = [];
            A = [];
            P = [];
            fixNum = [];
            mdl = cell(1, k);
            cv = cvpartition(size(rawData, 1), 'KFold', k);
        end

        if valSwitch == 1 
            % Simply train and predict
            if ~robustCheck
                M = fitlm(doneTrain, modelSpec,...
                    "ResponseVar", doneTrain.Properties.VariableNames{end}, ...
                    "PredictorVars", selectedNodes, 'CategoricalVars', catIdx);
            else
                M = fitlm(doneTrain, modelSpec,...
                    "ResponseVar", doneTrain.Properties.VariableNames{end}, ...
                    "PredictorVars", selectedNodes, 'RobustOpts', 'on', 'CategoricalVars', catIdx);
            end
            [Loss, rSquare, yPredict3, yActual3] = Prediction(M, doneVal);
        elseif valSwitch == 2
            % k-fold, RMSE use the median of sorted k RMSE
            for i = 1:cv.NumTestSets
                train = rawData(cv.training(i),:);
                val = rawData(cv.test(i),:);
                if needPCA && i == 1
                    [doneTrain, doneVal, fixNumNew] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, ...
                        fixNum, needFillOut);
                    fixNum = fixNumNew;
                elseif needPCA && i ~= 1
                    [doneTrain, doneVal, ~] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut);
                elseif ~needPCA 
                    [doneTrain, doneVal] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut);  
                end
                if ~robustCheck
                    M = fitlm(doneTrain, modelSpec,...
                        "ResponseVar", doneTrain.Properties.VariableNames{end}, ...
                        "PredictorVars", selectedNodes, 'CategoricalVars', catIdx);
                else
                    M = fitlm(doneTrain, modelSpec,...
                        "ResponseVar", doneTrain.Properties.VariableNames{end}, ...
                        "PredictorVars", selectedNodes, 'RobustOpts', 'on', 'CategoricalVars', catIdx);
                end
                [loss, r2, Predict, Actual] = Prediction(M, doneVal);
                rsquare = [rsquare r2];
                rmse = [rmse loss];
                A = [A; Actual];
                P = [P; Predict];
                mdl{i} = M;
            end
            yActual3 = A;
            yPredict3 = P;
            Loss = mean(rmse);
            rSquare = mean(rsquare);
        elseif valSwitch == 3
            % Simply train and predict
            if ~robustCheck
                M = fitlm(data, modelSpec,...
                    "ResponseVar", data.Properties.VariableNames{end}, ...
                    "PredictorVars", selectedNodes, 'CategoricalVars', catIdx);
            else
                M = fitlm(data, modelSpec,...
                    "ResponseVar", data.Properties.VariableNames{end}, ...
                    "PredictorVars", selectedNodes, 'RobustOpts', 'on', 'CategoricalVars', catIdx);
            end
            [Loss, rSquare, yPredict3, yActual3] = Prediction(M, valData);
        end
        % Actual model for testing (train with all data)
        if ~robustCheck
            Model = fitlm(data, modelSpec,...
                "ResponseVar", data.Properties.VariableNames{end}, ...
                "PredictorVars", selectedNodes, 'CategoricalVars', catIdx);
            if stepCheck
                Model = step(Model, 'NSteps', 5);
            end
        else
            Model = fitlm(data, modelSpec,...
                "ResponseVar", data.Properties.VariableNames{end}, ...
                "PredictorVars", selectedNodes, 'RobustOpts', 'on', 'CategoricalVars', catIdx);
        end
        % This is only for data distribution, not the real yPredict and
        % yActual
        [~, ~, yPredict, yActual] = Prediction(Model, data);
        figure(app.InfinityAI2ML)
    catch
        figure(app.InfinityAI2ML)
    end
%     When too many predictors, matlab would have a warning, remind the
%     user here
    [warnMsg, ~] = lastwarn;
    if ~isempty(warnMsg)
        warn = true;
    else
        warn = false;
    end
end