function [Model, Loss, rSquare, yPredict, yActual] = TrainSVR(app, data, response, rawData, selectedNodes, ...
    valSwitch, portion, k, valData, kernelType, boxConstraint, stridx, replacenan, normtype, needPCA, needFillOut, catIdx)
    % data : Whole dataset
    % valSwitch : Use different method to get validtion
    % feature: Selected two features in GUI
    % TrainPlotName: filename for saved figure
    % portion: number of validation data/ whole data
    % k: k-Fold
    % valData: loaded validation data 
    fixNum = [];
%     try
        if valSwitch == 1
            cv = cvpartition(size(rawData,1), 'HoldOut', portion);
            idx = cv.test;
            trainData = rawData(~idx, :);
            valData = rawData(idx, :);
            responseTrain = response(~idx, :);
            [doneTrain, doneVal] = ValPreprocess(trainData, valData, replacenan, normtype, needPCA, stridx, fixNum, ...
                needFillOut);
            if kernelType == 1
                Model = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint,'KernelFunction', 'linear', ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 2
                Model = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 2, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 3
                Model = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 3, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 4
                Model = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'gaussian', ...
                    'CategoricalPredictors', catIdx);
            end
            [Loss, rSquare, yPredict, yActual] = Prediction(Model, doneVal);
        elseif valSwitch == 2
            rmse = [];
            rsquare = [];
            predTotal = [];
            actTotal = [];
            cv = cvpartition(size(rawData, 1), 'KFold', k);
            for i = 1:cv.NumTestSets
                train = rawData(cv.training(i),:);
                val = rawData(cv.test(i),:);
                if needPCA && i == 1
                    [doneTrain, doneVal, fixNumNew] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, ...
                        fixNum, needFillOut);
                    fixNum = fixNumNew;
                elseif needPCA && i ~= 1
                    [doneTrain, doneVal, ~] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut);
                elseif ~needPCA 
                    [doneTrain, doneVal] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut); 
                end
                if kernelType == 1
                    M = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                        'BoxConstraint', boxConstraint,'KernelFunction', 'linear', ...
                        'CategoricalPredictors', catIdx);
                elseif kernelType == 2
                    M = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                        'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 2, ...
                        'CategoricalPredictors', catIdx);
                elseif kernelType == 3
                    M = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                        'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 3, ...
                        'CategoricalPredictors', catIdx);
                elseif kernelType == 4
                    M = fitrsvm(doneTrain, doneTrain.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                        'BoxConstraint', boxConstraint, 'KernelFunction', 'gaussian', ...
                        'CategoricalPredictors', catIdx);
                end
                [valRMSE, r2, yPredict, yActual] = Prediction(M, doneVal);
                predTotal = [predTotal; yPredict];
                actTotal = [actTotal; yActual];
                rmse = [rmse valRMSE];
                rsquare = [rsquare r2];
            end
            rSquare = mean(rsquare);
            rmse
            Loss = mean(rmse)
            yActual = actTotal;
            yPredict = predTotal;
        elseif valSwitch == 3
           if kernelType == 1
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint,'KernelFunction', 'linear', ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 2
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 2, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 3
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 3, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 4
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'gaussian', ...
                    'CategoricalPredictors', catIdx);
            end
            [Loss, rSquare, yPredict, yActual] = Prediction(Model, valData);
        end
        if valSwitch == 1 || valSwitch == 2
            if kernelType == 1
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint,'KernelFunction', 'linear', ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 2
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 2, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 3
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'polynomial', 'PolynomialOrder', 3, ...
                    'CategoricalPredictors', catIdx);
            elseif kernelType == 4
                Model = fitrsvm(data, data.Properties.VariableNames{end}, 'PredictorNames', selectedNodes, ...
                    'BoxConstraint', boxConstraint, 'KernelFunction', 'gaussian', ...
                    'CategoricalPredictors', catIdx);
            end
        end
        figure(app.InfinityAI2ML)
%     catch
%         figure(app.InfinityAI2ML)
%     end
end