function ScatterPlot(app, DataNoStr,response, plotType, predictorName, responseName, yActual, yPredict, date)
    % DataNoStr: data without string predictors, it'll be testTime when
    % predicting stock
    % response: response in  training data
    % plotType: 
        % 1 for data distribution before training 
        % 2 for data distribution after training 
        % 3 for Actual-by-predict plot
        % 4 for Run chart
    % predictorName: names of predictors
    % responseName: names of response
    % yActual: actual response
    % yPredict: predicted response
    % center: center value of training data(column by column)
    % scale: scale of training data(column by column)
    cla(app.UIAxes, 'reset')
    app.UIAxes.YLimMode = 'auto';
    app.UIAxes.XLimMode = 'auto';
    app.UIAxes.Visible = 'off';
    app.UIAxes.Visible = 'on';
    if plotType == 1
        % Distribution plot without training
        gscatter(app.UIAxes, DataNoStr, response);
        lgd = legend(app.UIAxes, 'Actual data');
        lgd.FontSize = 12;
    elseif plotType == 2
        % Data distribution
        group = ones(size(DataNoStr));
        gscatter(app.UIAxes, DataNoStr, response, group, 'r');
        hold(app.UIAxes, 'on' )
        if ~isempty(yPredict)
            group = ones(size(yPredict));
            gscatter(app.UIAxes, DataNoStr, yPredict, group, 'g')
            lgd = legend(app.UIAxes, 'Actual', 'Predict');
            lgd.FontSize = 12;
        end
        hold(app.UIAxes, 'off' )
    elseif plotType == 3
        % Actual-by-Predict plot
        group = ones(size(yActual));
        gscatter(app.UIAxes, yActual, yPredict, group, 'b');
        maxVal = [max(yActual) max(yPredict)];
        line(app.UIAxes, [0 max(maxVal)], [0 max(maxVal)], "Color", "k", 'LineWidth', 1)
        axis(app.UIAxes, [0 max(maxVal) 0 max(maxVal)])
        lgd = legend(app.UIAxes, 'Data', 'Perfect line');
        lgd.FontSize = 12;
    elseif plotType == 4
        % Run chart
        if app.LoaddataButtonGroup.SelectedObject == app.StockdataButton || ~isempty(app.ModelDir.Value)
            if isdatetime(date)
                x = datetime(date);
            else
                x = date;
            end
        else
            x = 1:size(yActual, 1);
        end
        plot(app.UIAxes, x, yActual, x, yPredict, 'LineWidth', 1)
        lgd = legend(app.UIAxes, 'Actual', 'Predict');
        lgd.FontSize = 12;
        app.UIAxes.XLim = [x(1) x(end)];
        grid(app.UIAxes, 'on');
    end
    xlabel(app.UIAxes, predictorName, 'FontSize', 16, 'Interpreter','none');
    ylabel(app.UIAxes, responseName, 'FontSize', 16, 'Interpreter','none');
    disableDefaultInteractivity(app.UIAxes)
end