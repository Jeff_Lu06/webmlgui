function [doneTrain, doneVal, fixNumNew] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
    needFillOut)
    numidx = setdiff(1:size(train, 2)-1, stridx);
    % Fill outliers
%     if needFillOut
%         idx = [];
%         tempData = train(:, 1:end-1);
%         [tempData(:, numidx), ~, lowTh, highTh, med] = filloutliers(tempData(:, numidx), 'center');
%         % Avoid circustances when lowTh = highTh, causing columns become the same number 
%         for i = 1:numel(lowTh)
%             if lowTh{1, i} == highTh{1, i}
%                 idx = [idx i];
%             end
%         end
%         if ~isempty(idx)
%             train(:, [1:idx-1 idx+1:end-1]) = tempData(:, [1:idx-1 idx+1:end]);
%         end
%         tempVal = val(:, numidx);
%         for i = 1:size(tempVal, 2)-1
%             col = tempVal{:, i};
%             idx1 = col<lowTh{1, i};
%             idx2 = col>highTh{1, i};
%             col(idx1) = med{1, i};
%             col(idx2) = med{1, i};
%             val{:, i} = col;
%         end
%     end
    % Fill nan string feature
    for i = 1:numel(stridx)
        col = train{:, stridx(i)};
        colVal = val{:, stridx(i)};
        % In order to use fillmissing(), transfer to category
        col = categorical(col);
        colVal = categorical(colVal);
        freqStr = mode(col);
        % Replace nan value with the most frequent string
        col = fillmissing(col, 'constant', freqStr);
        colVal = fillmissing(colVal, 'constant', freqStr);
        train{:, stridx(i)} = col;
        val{:, stridx(i)} = colVal;
    end
    % Fill nan numeric feature in training set
    if replacenan == 1
        train{:, numidx} = fillmissing(train{:, numidx}, 'movmedian', size(train{:, numidx}, 1)*2, 1);
        freqNum = median(train{:, numidx});
    elseif replacenan == 2
        train{:, numidx} = fillmissing(train{:, numidx}, 'movmean', size(train{:, numidx}, 1)*2, 1);
        freqNum = mean(train{:, numidx});
    end
    % Fill nan numeric feature in validation set
    for i = 1:numel(numidx)
        col = val{:, numidx(i)};
        col = fillmissing(col, 'constant', freqNum(i));
        val{:, numidx(i)} = col;
    end
    % Normalize
    if normtype ~= false
        % Training set
        [train(:,numidx), center, scale] = normalize(train(:,numidx), normtype);
        % Validation set
        val(:,numidx) = normalize(val(:,numidx), 'c', center, 's', scale);
    end
    % Do PCA separately
    if needPCA 
        component = train{:, 1:end-1};
        componentVal = val{:, 1:end-1};
        [COEFF, ~, latent] = pca(component);
        % Select feature number that represents 95% of original data
        % Set ForwardNum = 2 when clustering
        SelectNum = cumsum(latent)./sum(latent);
        index = find(SelectNum >= 0.95);
        if isempty(fixNum)
            ForwardNum = index(1);
            fixNumNew = ForwardNum;
        else
            ForwardNum = fixNum;
            fixNumNew = fixNum;
        end
        % Create transformation matrix
        transMat = COEFF(:, 1:ForwardNum);
        component = component * transMat;
        componentVal = componentVal * transMat;
        component = array2table(component);
        doneTrain = [component train(:, end)];
        componentVal = array2table(componentVal);
        componentVal.Properties.VariableNames(1:end) = component.Properties.VariableNames(1:end);
        doneVal = [componentVal val(:, end)];
    else
        doneTrain = train;
        doneVal = val;
    end
end