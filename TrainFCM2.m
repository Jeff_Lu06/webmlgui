function [resultIter, prediction, silhouetteAvgIdex, center, strClust] = TrainFCM2(app, data, numCluster, maxIter, improvement, selectedFeature, needplot)
    app.UIAxes.YLimMode = 'auto';
    app.UIAxes.XLimMode = 'auto';
    app.UIAxes.Visible = 'off';
    app.UIAxes.Visible = 'on';
    cla(app.UIAxes, 'reset')
    disableDefaultInteractivity(app.UIAxes)
    warning off;
    if ~needplot || size(data, 2) == 1
        app.PlotSwitchGroup.SelectedObject = app.DatadistributionButton;
        app.Label.Visible = 'off';
        app.Label_2.Visible = 'off';
        app.UIAxes.Visible = 'on';
        app.Panel_3.Visible = 'off';
    end
    
    X = table2array(data);
    [n, no] = size(X);
    J = zeros(length(numCluster), maxIter);
    m = 1.5;
    prediction = zeros(length(numCluster), n);
    silhouetteAvgIdex = zeros(1, length(numCluster));
    resultIter = zeros(1, length(numCluster));
    center = cell(1, length(numCluster));
    f1 = selectedFeature(1);
    f2 = selectedFeature(2);
    rng(1)

    for xx = 1:length(numCluster)
        c = numCluster(xx);
        v = repmat(max(X), c, 1).*rand([c, no]);
        U = rand([c, n]);
        strClust = cell(1, numCluster(xx));
        for i = 1:numCluster(xx)
            str = strcat('Clust ', string(i));
            strClust{i} = str;
        end
        
        % Initialize center and member ship value
        for j = 1:n
              U(:, j) = U(:, j)./sum(U(:, j));      
        end  

        for i = 1:c
              v(i, :) = sum((X(:, :).*repmat(U(i, :)'.^m, 1, no)),1)./sum(U(i, :).^m);
        end
        delta = 1e4;
        yy = 1;

        while  (yy < maxIter && delta > improvement)
            % Update center and member ship value
            for i = 1:c
              for j = 1:n
                  sum1 = 0;
                  for k = 1:c
                    temp = (norm(X(j, :) - v(i, :)) ./ norm(X(j, :) - v(k, :))).^(2/(m-1));
                    sum1 = sum1 + temp;
                  end
                  U(i, j) = 1./sum(sum1);
              end
            end
            
            for i = 1:c
               v(i, :) = sum((X(:, :).*repmat(U(i, :)'.^m, 1, no)), 1)./sum(U(i, :).^m);  
            end
            
            % Calculate objective function
            temp1 = zeros(c,n);
            for j = 1:c
                for k = 1:n
                    temp1(j,k) = U(j,k)^m*(norm(X(k, :) - v(j, :)))^2;
                end
            end
            J(xx, yy) = sum(sum(temp1)); 
            [~, label] = max(U);
            prediction(xx, :) = label;
            
            % Plot
            if needplot && size(X, 2) ~= 1
                gscatter(app.UIAxes, X(:, f1), X(:, f2), prediction(xx, :))
                xlabel(app.UIAxes, data.Properties.VariableNames{1}, 'FontSize', 12);
                ylabel(app.UIAxes, data.Properties.VariableNames{2}, 'FontSize', 12);
                hold(app.UIAxes, 'on');
                plot(app.UIAxes, v(:, f1), v(:, f2), 'k*')
                legend(app.UIAxes, [strClust 'Centroid'])
                app.UIAxes.Title.String = strcat('Current cluster = ', string(numCluster(xx)), ', Number of iteration = ', string(yy));
                hold(app.UIAxes, 'off');
                pause(0.001)
            end
            if yy > 1
                delta = abs(J(xx, yy-1) - J(xx, yy));
            end
            yy = yy+1;
        end
        resultIter(xx) = yy - 1;
        center{xx} = v;
        
        % Calculate silhouette index
        E = silhouette(X, prediction(xx, :));
        silhouetteAvgIdex(xx) = mean(E);
    end
    app.UIAxes.Title.String = "";
    disableDefaultInteractivity(app.UIAxes)
    app.UIAxes.Toolbar.Visible = 'off';
end