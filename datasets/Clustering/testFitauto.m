clear; close all; clc;
data = readtable('CC GENERAL.csv');
% [ModelASHA, inofASHA] = fitcauto(data, data.Properties.VariableNames{end}, "HyperparameterOptimizationOptions", ...
%     struct("Optimizer","asha"));

% [Model, inof] = fitcauto(data, data.Properties.VariableNames{end});

[forest,tf,scores] = iforest(data);
count = nnz(tf)