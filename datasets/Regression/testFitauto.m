clear; close all; clc;
data = readtable('insurance.csv');
data = data(:, 1:end-1);
% Test fitcauto
% [ModelASHA, inofASHA] = fitcauto(data, data.Properties.VariableNames{end}, "HyperparameterOptimizationOptions", ...
%     struct("Optimizer","asha"));
% [Model, inof] = fitcauto(data, data.Properties.VariableNames{end});

% Test iforest for finding outliers
% [forest,tf,scores] = iforest(data);
% count = nnz(tf)

% Test feature ranking 
[idx, score] = fscmrmr(data, data.Properties.VariableNames{end});