clear; close all; clc;

data = readtable('phishTrain.csv');
catIdx = zeros(1, size(data, 2)-1);
j = 1;
for i = 1:size(data, 2)-1
    col = data{:, i};
    uCol = unique(col);
    if uCol == 1
        catIdx(j) = i;
        j = j + 1;
        continue
    end
    if length(uCol) < 10
        catIdx(j) = i;
        j = j + 1;
    end
end
catIdx(j:end) = [];