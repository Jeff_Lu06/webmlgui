function [RMSE, rSquare, yPredict, yActual] = Prediction(Model, data)
    yPredict = predict(Model, data);
    yActual = data{:,end};
    % Examine NAN and inf in yPredict
    invalidIndex = [find(isnan(yPredict)); find(isnan(yActual))];
    p = yPredict;
    a = yActual;
    p(invalidIndex) = [];
    a(invalidIndex) = [];
    residual = a - p;
    RMSE = sqrt(mean(residual.^2));
    SSE = sum(residual.^2);
    SST = sum((p - mean(a)).^2);
    rSquare = 1-(SSE/SST);
end