function [stock, ticker] = getStockForGUI(start_date, end_date, ticker)

origDate = datenum('1970-Jan-01 00:00:00', 'yyyy-mm-dd HH:MM:SS');
% Convert input dates to Matlab datenums, if necessary
startDate = (datenum(start_date, 'yyyymmdd') - origDate) * 24 * 60 * 60;
endDate = (datenum(end_date, 'yyyymmdd') - origDate) * 24 * 60 * 60;

% determine if user specified frequency
freq = 'd';                             % default is daily

% determine if user specified event type
event = 'history';                      % default is historical prices

% Get historical data
url1 = sprintf(['https://query1.finance.yahoo.com/v7/finance/download/', ...
    '%s?period1=%d&period2=%d&interval=1%s&events=%s'], ...
    ticker, startDate, endDate, freq, event);

% Create webread options variable to specify UserAgent
options = weboptions('UserAgent', 'Mozilla/5.0');

% Try to download stock data. Put in try/catch block in case it fails
try
    stock = webread(url1, 'matlabstockdata@yahoo.com', 'historical stocks', options);

% If data retrieval fails, skip to next ticker
catch
    try
        ticker2 = [ticker 'O'];
        url2 = sprintf(['https://query1.finance.yahoo.com/v7/finance/download/', ...
            '%s?period1=%d&period2=%d&interval=1%s&events=%s'], ...
            ticker2, startDate, endDate, freq, event);
        stock = webread(url2, 'matlabstockdata@yahoo.com', 'historical stocks', options);
        ticker = ticker2;
    catch
        ticker = ticker2;
        warn = warndlg('Unable to get data, please check your network connection status.');
        waitfor(warn)
    end
end