clear;close all;clc;

tic
tempdata=  readtable('E:\Jeff\ML GUI\datasets\insurance.csv');
toc
nanCol = [];
strindex = [];
numindex = [];
invalidCount = [];
max_cat = [];
tempLabels = tempdata{:,end};
% Delete data with invalid label
% if nnz(~isfinite(tempLabels)) ~= 0
%     tempdata(~isfinite(tempLabels), :) = [];
% end
DataNoString = tempdata;
% Examine data column by column
tic
for i = 1:size(tempdata, 2)
    col = tempdata{:, i};
    if isnumeric(col)
        numindex = [numindex i];
        numInvalidData = nnz(~isfinite(col));
        colMean = mean(col(isfinite(col)));
        % Get columns index with too many invalid data
        if numInvalidData > 0.3*numel(col)
            nanCol = [nanCol i];
        end
        invalidCount = [invalidCount numInvalidData];
        % Replace invalid data with mean of that column
        col(~isfinite(col)) = colMean;
        tempdata{:, i} = col;
    else
        % If it's a string column, transform it into numeric column
        strindex = [strindex i];
        max_cat = [max_cat numel(unique(col))];
        col = categorical(col);
        freqCol = mode(col);
        col = fillmissing(col, 'constant', freqCol);
        newCol = grp2idx(col);
        VarName = DataNoString.Properties.VariableNames{i};
        DataNoString.(VarName) = newCol;
    end
end
toc
% Get str-to-numeric chart
tic
str2numChart = cell(max(max_cat), numel(strindex)+1);
str2numChart(:) = {0};
for i = 1:numel(strindex)+1
    if i ~= numel(strindex)+1
        tempCol = unique(tempdata{:, strindex(i)});
        for j = 1:numel(tempCol)
            str2numChart{j, i} = tempCol{j};
        end
    else
        for j = 1:size(str2numChart, 1)
            str2numChart{j, i} = j;
        end
    end
end
toc