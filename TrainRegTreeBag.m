function [Model, Loss, rSquare, yPredict, yActual, yPredict3, yActual3] = TrainRegTreeBag(app, data, rawData, ...
    selectedNodes, valSwitch, portion, k, valData, numTrees, numMaxSplits, stridx, replacenan, ...
    normtype, needPCA, needFillOut, catIdx)
    % data: training data
    % selectedNodes: selected predictors for training
    % valSwitch: use different methods to get validtion
        % valSwitch = 1, use partial training data as validation data
        % valSwitch = 2, use k-fold
        % valSwitch = 3, use uploaded validation data
    % portion: number of validation data/ whole data
    % k: k-fold
    % valData: loaded validation data 
% Model parameters:
    % modelSpec: terms, like linear, interactions, quadratic
    % stridx: index of string predictors
    % Create wait bar and keep it running
    lastwarn('');
    fixNum = [];
    try
        % Set up parameters for each validation method
        if valSwitch == 1
            cv = cvpartition(size(rawData, 1),'HoldOut', portion);
            idx = cv.test;
            trainData = rawData(~idx, :);
            valData = rawData(idx, :);
            [doneTrain, doneVal] = ValPreprocess(trainData, valData, replacenan, normtype, needPCA, stridx, fixNum, ...
                needFillOut);
        elseif valSwitch == 2
            rmse = [];
            rsquare = [];
            A = [];
            P = [];
            fixNum = [];
            mdl = cell(1, k);
            cv = cvpartition(size(rawData, 1), 'KFold', k);
        end

        if valSwitch == 1 
            % Simply train and predict
            M = TreeBagger(numTrees, doneTrain, doneTrain.Properties.VariableNames{end}, ...
                'MaxNumSplits', numMaxSplits, 'Method', 'regression', ...
                'PredictorNames', selectedNodes, 'CategoricalPredictors', catIdx);
            [Loss, rSquare, yPredict3, yActual3] = Prediction(M, doneVal);
        elseif valSwitch == 2
            % k-fold, RMSE use the median of sorted k RMSE
            for i = 1:cv.NumTestSets
                train = rawData(cv.training(i),:);
                val = rawData(cv.test(i),:);
                if needPCA && i == 1
                    [doneTrain, doneVal, fixNumNew] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, ...
                        fixNum, needFillOut);
                    fixNum = fixNumNew;
                elseif needPCA && i ~= 1
                    [doneTrain, doneVal, ~] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut);
                elseif ~needPCA 
                    [doneTrain, doneVal] = ValPreprocess(train, val, replacenan, normtype, needPCA, stridx, fixNum, ...
                        needFillOut);  
                end
                M = TreeBagger(numTrees, doneTrain, doneTrain.Properties.VariableNames{end}, ...
                'MaxNumSplits', numMaxSplits, 'Method', 'regression', ...
                'PredictorNames', selectedNodes, 'CategoricalPredictors', catIdx);
                [loss, r2, Predict, Actual] = Prediction(M, doneVal);
                rsquare = [rsquare r2];
                rmse = [rmse loss];
                A = [A; Actual];
                P = [P; Predict];
                mdl{i} = M;
            end
            yActual3 = A;
            yPredict3 = P;
            Loss = mean(rmse);
            rSquare = mean(rsquare);
        elseif valSwitch == 3
            % Simply train and predict
            M = TreeBagger(numTrees, data, data.Properties.VariableNames{end}, ...
                'MaxNumSplits', numMaxSplits, 'Method', 'regression', ...
                'PredictorNames', selectedNodes, 'CategoricalPredictors', catIdx);
            [Loss, rSquare, yPredict3, yActual3] = Prediction(M, valData);
        end
        % Actual model for testing (train with all data)
        Model = TreeBagger(numTrees, data, data.Properties.VariableNames{end}, ...
                'MaxNumSplits', numMaxSplits, 'Method', 'regression', ...
                'PredictorNames', selectedNodes, 'CategoricalPredictors', catIdx);
        % This is only for data distribution, not the real yPredict and
        % yActual
        [~, ~, yPredict, yActual] = Prediction(Model, data);
        figure(app.InfinityAI2ML)
    catch
        figure(app.InfinityAI2ML)
    end
end